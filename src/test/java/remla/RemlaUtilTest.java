package remla;

import static org.junit.Assert.assertFalse;

import org.junit.Test;

public class RemlaUtilTest {

	@Test
	public void ensureNonEmptyHostName() {
		String actual = RemlaUtil.getHostName();
		assertFalse(actual == null || actual.isEmpty());
	}

	@Test
	public void ensureNonEmptyVersion() {
		String actual = RemlaUtil.getVersion();
		assertFalse(actual == null || actual.isEmpty());
	}
}